import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Home from './pages/Home/Home';
import About from './pages/About/About';

class App extends React.Component {
    render() {
        return (
            <div className="container">
                <Router>
                    <Header />
                    <Switch>
                        <Route exact path="/"><Home /></Route>
                        <Route path="/about"><About /></Route>
                    </Switch>
                    <Footer />
                </Router>
            </div>
        ); 
    }
};

ReactDOM.render(<App />, document.getElementById('root'));