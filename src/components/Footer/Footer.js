import React from 'react';
import {BrowserRouter as Router, Link} from "react-router-dom";

class Footer extends React.Component {
    render() {
        return (
            <footer className='footer'>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/about">About</Link></li>
                </ul>
            </footer>
        );
    }
}

export default Footer;