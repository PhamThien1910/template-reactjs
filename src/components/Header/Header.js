import React from 'react';
import {BrowserRouter as Router, Link} from "react-router-dom";
import styles from './Header.scss';
import logo from './logo.svg';

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <div>
                    <h1 className="header__logo"><Link to="/"><img src={logo} alt="logo" width="80" height="80" /></Link></h1>
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/about">About</Link></li>
                    </ul>
                </div>
            </header>
        );
    }
}

export default Header;